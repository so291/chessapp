package com.example.chessapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Array;
import java.io.*;
import java.util.ArrayList;

public class popSaveGame extends Chess {
    ListView saveList;
    final ArrayList<saveState> names = new ArrayList<saveState>();
    final String FILE_NAME = "Save_List.txt";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popsavewindow);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int w = dm.widthPixels;
        int h = dm.heightPixels;
        getWindow().setLayout((int) (w * 0.8), (int) (h * 0.8));

        final String[][] passedString_list = (String[][]) getIntent().getExtras().getSerializable("board");

        final Button saveButton = (Button) findViewById(R.id.SaveButton);
        saveList = (ListView) findViewById(R.id.SaveNameList);
        final TextView error = (TextView) findViewById(R.id.Error);
        final EditText saveName = (EditText) findViewById(R.id.save_Name);

        FileInputStream fis = null;
        try {
            fis=openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            names.clear();
            for (String text = br.readLine(); text != null; text = br.readLine()) {
                saveState newSaveState = new saveState(text,text+".txt");
                names.add(newSaveState);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        final SaveListAdapter adapter = new SaveListAdapter(this, R.layout.rowadapterview, names);
        saveList.setAdapter(adapter);

        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String potSaveName = saveName.getText().toString();
                if (names.contains(potSaveName)) {
                    error.setVisibility(View.VISIBLE);
                } else {
                    FileOutputStream fos = null;
                    try {
                        fos=openFileOutput(FILE_NAME, Context.MODE_APPEND);
                        fos.write((potSaveName+"\n").getBytes());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally{
                        if(fos != null){
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    saveFile(passedString_list,potSaveName);
                    saveState newSaveState = new saveState(potSaveName,potSaveName+".txt");

                    names.add(newSaveState);
                    adapter.notifyDataSetChanged();
                    error.setVisibility(View.INVISIBLE);
                }
            }
        });

        saveList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[][] tagsList = new String[8][8];
                String file_Name = names.get(position).url;
                FileInputStream fis = null;
                try {
                    fis=openFileInput(file_Name);
                    InputStreamReader isr = new InputStreamReader(fis);
                    BufferedReader br = new BufferedReader(isr);
                    for(int i=0; i<8; i++){
                        for(int j=0; j<8; j++){
                            String text = br.readLine();
                            Log.d("popSaveGame", text);
                            tagsList[i][j] = text;
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if(fis != null){
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                Bundle b = new Bundle();
                b.putSerializable("board", tagsList);
                Intent i = new Intent(popSaveGame.this, Chess.class);
                i.putExtras(b);
                startActivity(i);
            }
        });
    }
    public void saveFile(String temp[][], String name){
        FileOutputStream fos = null;
        name += ".txt";
        try {
            fos = openFileOutput(name, MODE_PRIVATE);
            for(int i=0; i<8; i++){
                for(int j=0; j<8; j++){
                    fos.write((temp[i][j]+"\n").getBytes());
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
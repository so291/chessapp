package com.example.chessapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.util.Log;
import java.lang.Math;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Chess extends AppCompatActivity implements View.OnClickListener{

    private Button[][] buttons = new Button[8][8];

    private boolean whiteTurn=true;
    private boolean firstPartOfTurn=true;
    private boolean checkMate=false;
    private boolean drawOffer=false;
    private TextView displayTextView;
    String winner = "Checkmate";

    int StartX=0;
    int StartY=0;
    int EndX=0;
    int EndY=0;
    int KingX;
    int KingY;

   String prevTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board);


        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                buttons[i][j].setOnClickListener(this);
            }
        }

        Intent intent = getIntent();
        String[][] SaveState = new String[8][8];
        if (intent.hasExtra("board")) {
            SaveState = (String[][]) getIntent().getExtras().getSerializable("board");
            for(int i=0; i<8; i++){
                for(int j=0; j<8; j++){
                    if(SaveState[i][j].equalsIgnoreCase("blackbishop")){
                        buttons[i][j].setTag("blackbishop");
                        buttons[i][j].setBackgroundResource(R.drawable.blackbishop);
                    } else if(SaveState[i][j].equalsIgnoreCase("blackking")){
                        buttons[i][j].setTag("blackking");
                        buttons[i][j].setBackgroundResource(R.drawable.blackking);
                    } else if(SaveState[i][j].equalsIgnoreCase("blackknight")){
                        buttons[i][j].setTag("blackknight");
                        buttons[i][j].setBackgroundResource(R.drawable.blackknight);
                    } else if(SaveState[i][j].equalsIgnoreCase("blackpawn")) {
                        buttons[i][j].setTag("blackpawn");
                        buttons[i][j].setBackgroundResource(R.drawable.blackpawn);
                    } else if(SaveState[i][j].equalsIgnoreCase("blackqueen")){
                        buttons[i][j].setTag("blackqueen");
                        buttons[i][j].setBackgroundResource(R.drawable.blackqueen);
                    } else if(SaveState[i][j].equalsIgnoreCase("blackrook")){
                        buttons[i][j].setTag("blackrook");
                        buttons[i][j].setBackgroundResource(R.drawable.blackrook);
                    } else if(SaveState[i][j].equalsIgnoreCase("blacktile")){
                        buttons[i][j].setTag("blacktile");
                        buttons[i][j].setBackgroundResource(R.drawable.blacktile);
                    } else if(SaveState[i][j].equalsIgnoreCase("whitebishop")){
                        buttons[i][j].setTag("whitebishop");
                        buttons[i][j].setBackgroundResource(R.drawable.whitebishop);
                    } else if(SaveState[i][j].equalsIgnoreCase("whiteking")){
                        buttons[i][j].setTag("whiteking");
                        buttons[i][j].setBackgroundResource(R.drawable.whiteking);
                    } else if(SaveState[i][j].equalsIgnoreCase("whiteknight")){
                        buttons[i][j].setTag("whiteknight");
                        buttons[i][j].setBackgroundResource(R.drawable.whiteknight);
                    } else if(SaveState[i][j].equalsIgnoreCase("whitepawn")){
                        buttons[i][j].setTag("whitepawn");
                        buttons[i][j].setBackgroundResource(R.drawable.whitepawn);
                    } else if(SaveState[i][j].equalsIgnoreCase("whitequeen")){
                        buttons[i][j].setTag("whitequeen");
                        buttons[i][j].setBackgroundResource(R.drawable.whitequeen);
                    } else if(SaveState[i][j].equalsIgnoreCase("whiterook")){
                        buttons[i][j].setTag("whiterook");
                        buttons[i][j].setBackgroundResource(R.drawable.whiterook);
                    } else if(SaveState[i][j].equalsIgnoreCase("whitetile")){
                        buttons[i][j].setTag("whitetile");
                        buttons[i][j].setBackgroundResource(R.drawable.whitetile);
                    }
                }
            }
        }

        Button undoButton = findViewById(R.id.undoButton);
        Button SaveButton = findViewById(R.id.SaveButton);
        Button AIButton = findViewById(R.id.button_AI);
        Button DrawButton = findViewById(R.id.button_Draw);
        Button ResignButton = findViewById(R.id.button_Resign);
        displayTextView = findViewById(R.id.text_view_display_text);

        AIButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean completedMove=false;
                if(whiteTurn){
                    for(int i=0;i<8;i++){
                        for(int j=0;j<8;j++){
                            if(buttons[i][j].getTag().toString().contains("white")&&!buttons[i][j].getTag().toString().contains("tile")){
                                for(int m=0; m<8; m++){
                                    for(int n=0;n<8;n++){
                                        if(validMove(i, j, m, n)&&checkPath(i, j, m, n)&&!completedMove){
                                            completedMove=true;
                                            prevTag=buttons[m][n].getTag().toString();
                                            buttons[m][n].setBackground(buttons[i][j].getBackground());
                                            buttons[m][n].setTag(buttons[i][j].getTag());
                                            if (((i + j) % 2) == 0) {
                                                buttons[i][j].setBackgroundResource(R.drawable.whitetile);
                                                buttons[i][j].setTag("whitetile");
                                            } else {
                                                buttons[i][j].setBackgroundResource(R.drawable.blacktile);
                                                buttons[i][j].setTag("blacktile");
                                            }
                                            whiteTurn=!whiteTurn;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    for(int i=0;i<8;i++){
                        for(int j=0;j<8;j++){
                            if(buttons[i][j].getTag().toString().contains("black")&&!buttons[i][j].getTag().toString().contains("tile")){
                                for(int m=0; m<8; m++){
                                    for(int n=0;n<8;n++){
                                        if(validMove(i, j, m, n)&&checkPath(i, j, m, n)&&!completedMove){
                                            completedMove=true;
                                            prevTag=buttons[m][n].getTag().toString();
                                            buttons[m][n].setBackground(buttons[i][j].getBackground());
                                            buttons[m][n].setTag(buttons[i][j].getTag());
                                            if (((i + j) % 2) == 0) {
                                                buttons[i][j].setBackgroundResource(R.drawable.whitetile);
                                                buttons[i][j].setTag("whitetile");
                                            } else {
                                                buttons[i][j].setBackgroundResource(R.drawable.blacktile);
                                                buttons[i][j].setTag("blacktile");
                                            }
                                            whiteTurn=!whiteTurn;
                                        }
                                    }
                                }
                            }
                        }

                    }}
            }
        });

        DrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawOffer) {
                    displayTextView.setText("Draw accepted, the game is over");
                }
                if(!drawOffer){
                    drawOffer=true;
                    if(whiteTurn) {
                        displayTextView.setText("White offers draw. Black: click to accept the draw.");
                    }
                    else{
                        displayTextView.setText("Black offers draw. White: click to accept the draw.");
                    }
                }


            }
        });

        ResignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(whiteTurn){
                    displayTextView.setText("White resigns; black wins!");
                }
                else{
                    displayTextView.setText("Black resigns; white wins!");
                }
            }
        });

        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!firstPartOfTurn){
                    firstPartOfTurn=true;
                }
                else{
                    buttons[StartX][StartY].setBackground(buttons[EndX][EndY].getBackground());
                    buttons[StartX][StartY].setTag(buttons[EndX][EndY].getTag());
                    for(int i =0; i<8;i++){
                        for(int j=0;j<8;j++){
                            if(buttons[i][j].getTag().toString().contentEquals(prevTag)){
                                buttons[EndX][EndY].setBackground(buttons[i][j].getBackground());
                                buttons[EndX][EndY].setTag(buttons[i][j].getTag());
                            }
                        }
                    }
                    whiteTurn=!whiteTurn;
                }
            }
        });

        SaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[][] board = new String[8][8];
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        board[i][j] = buttons[i][j].getTag().toString();
                        Log.d("Button Tags", buttons[i][j].getTag().toString());
                    }
                }
                Bundle b = new Bundle();
                b.putSerializable("board", board);
                Intent i = new Intent(Chess.this, popSaveGame.class);
                i.putExtras(b);
                startActivity(i);
            }
        });

    }
    @Override
    public void onClick(View v) {


        if (firstPartOfTurn) {
            if (whiteTurn) {
                if (v.getTag().toString().contains("black") || v.getTag().toString().contains("tile")) {
                    return;
                }
            }
            else if (v.getTag().toString().contains("white") || v.getTag().toString().contains("tile")) {
                return;
            }
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        if (buttons[i][j].getId() == v.getId()) {
                            StartX = i;
                            StartY = j;
                        }
                    }
                }

                //Log.d("tag", Integer.toString(tempButtonID));
                firstPartOfTurn = false;
            } else {

            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (buttons[i][j].getId() == v.getId()) {
                        EndX = i;
                        EndY = j;
                    }
                }
            }
            if(validMove(StartX, StartY, EndX, EndY)&&checkPath(StartX, StartY, EndX, EndY)) {
                prevTag=v.getTag().toString();

                v.setBackground(buttons[StartX][StartY].getBackground());
                v.setTag(buttons[StartX][StartY].getTag());



                if (((StartX + StartY) % 2) == 0) {
                    buttons[StartX][StartY].setBackgroundResource(R.drawable.whitetile);
                    buttons[StartX][StartY].setTag("whitetile");
                } else {
                    buttons[StartX][StartY].setBackgroundResource(R.drawable.blacktile);
                    buttons[StartX][StartY].setTag("blacktile");
                }
                if(isBlack(buttons[EndX][EndY])){
                    for (int i = 0; i < 8; i++) {
                        for (int j = 0; j < 8; j++) {
                            if (buttons[i][j].getTag().toString().contentEquals("whiteking")) {
                                KingX = i;
                                KingY = j;
                            }
                        }
                    }
                    if(validMove(EndX, EndY, KingX, KingY)&&checkPath(EndX, EndY, KingX, KingY)){
                        displayTextView.setText("Check!");
                        checkMate=true;
                        for(int i=-1;i<=1;i++){
                            for(int j=-1;j<=1;j++){
                                if(KingX+i<8&&KingX+i>-1&&KingY+j<8&&KingY+j>-1){
                                    if(!(validMove(EndX, EndY, KingX+i, KingY+j)&&checkPath(EndX, EndY, KingX+i, KingY+j))&&!isOccupied(buttons[KingX+i][KingY+j])){
                                        checkMate=false;
                                    }
                                }
                            }
                        }
                        if(checkMate){
                            if(whiteTurn){
                                String winner = "Checkmate, " + "\nwhite wins!";
                                displayTextView.setText(winner);
                            }
                            else{
                                String winner = "Checkmate, " + "\nblack wins!";
                                displayTextView.setText(winner);
                            }
                        }
                    }
                }
                else{
                    for (int i = 0; i < 8; i++) {
                        for (int j = 0; j < 8; j++) {
                            if (buttons[i][j].getTag().toString().contentEquals("blackking")) {
                                KingX = i;
                                KingY = j;
                            }
                        }
                    }
                    if(validMove(EndX, EndY, KingX, KingY)&&checkPath(EndX, EndY, KingX, KingY)){
                        displayTextView.setText("Check!");
                        checkMate=true;
                        for(int i=-1;i<=1;i++){
                            for(int j=-1;j<=1;j++){
                                if(KingX+i<8&&KingX+i>-1&&KingY+j<8&&KingY+j>-1){
                                    if(!(validMove(EndX, EndY, KingX+i, KingY+j)&&checkPath(EndX, EndY, KingX+i, KingY+j))&&!isOccupied(buttons[KingX+i][KingY+j])){
                                        checkMate=false;
                                    }
                                }
                            }
                        }
                        if(checkMate){
                            if(whiteTurn){
                                String winner = "Checkmate, white wins!";
                                displayTextView.setText(winner);
                            }
                            else{
                                String winner = "Checkamte, black wins!";
                                displayTextView.setText(winner);
                            }
                        }
                    }
                }
                firstPartOfTurn = true;
                whiteTurn = !whiteTurn;
            }
            else{
                return;
            }
            }
    }
    public boolean isOccupied(Button b) {
        if(b.getTag().toString().contains("tile")) {
            return false;
        }
        return true;
    }

    public boolean validMove(int x1, int y1, int x2, int y2){
        if((x1==x2)&&(y1==y2)){
            return false;
        }
        if(buttons[x1][y1].getTag().toString().contains("pawn")){
            if(buttons[x1][y1].getTag().toString().contains("black")){
                if(x1==1){
                    if(y2==y1 && ((x2==(x1+1))||(x2==x1+2))){
                        return true;
                    }
                }
                if((x2==(x1+1))&& y2==y1){
                    return true;
                }
                if(x2==(x1+1)){
                    if(Math.abs(y2-y1)==1){
                        return true;
                    }
                }
                return false;
            }
            else{
                if(x1==6){
                    if(y2==y1 && ((x2==(x1-1))||(x2==x1-2))){
                        return true;
                    }
                }
                if((x2==(x1-1))&& y2==y1){
                    return true;
                }
                if(x2==(x1-1)){
                    if(Math.abs(y2-y1)==1){
                        return true;
                    }
                }
                return false;
            }
        }
        if(buttons[x1][y1].getTag().toString().contains("rook")){
            if(x1!=x2){
                if(y1!=y2){
                    return false;
                }
                return true;
            }
            if(y2!=y1){
                if(x1!=x2){
                    return false;
                }
                return true;
            }
        }

        if(buttons[x1][y1].getTag().toString().contains("knight")){
            if(Math.abs(x1-x2)<3){
                if(Math.abs(y1-y2)<2) {
                    return true;
                }
            }
            if(Math.abs(y1-y2)<2){
                if(Math.abs(x1-x2)<3){
                    return true;
                }
            }
            if(Math.abs(x2-x1)==1){
                if(Math.abs(y2-y1)==2){
                    return true;
                }
            }
            if(Math.abs(y2-y1)==2){
                if(Math.abs(x2-x1)==1){
                    return true;
                }
            }
            return false;
        }

        if(buttons[x1][y1].getTag().toString().contains("bishop")){
            if(Math.abs(x1-x2)==Math.abs(y1-y2)){
                return true;
            }
            return false;
        }
        if(buttons[x1][y1].getTag().toString().contains("queen")){
            if(Math.abs(x1-x2)==Math.abs(y1-y2)){
                return true;
            }
            if(x1!=x2){
                if(y1!=y2){
                    return false;
                }
                return true;
            }
            if(y2!=y1){
                if(x1!=x2){
                    return false;
                }
                return true;
            }
        }
        if(buttons[x1][y1].getTag().toString().contains("king")){
            if(Math.abs(x2-x1)<=1){
                if(Math.abs(y2-y1)<=1){
                    return true;
                }
                return false;
            }
            return false;
        }

            return true;

    }

    public boolean checkPath(int X1, int Y1, int X2, int Y2){
        if(isBlack(buttons[X1][Y1])==isBlack(buttons[X2][Y2])){
            if(!isTile(buttons[X2][Y2])){
                return false;
            }
        }
        if(buttons[X1][Y1].getTag().toString().contains("pawn")){
            if(Y2==Y1) {
                int spacesMoved = Math.abs(X1 - X2);
                if (isBlack(buttons[X1][Y1])) {
                    for (int i = 1; i <= spacesMoved; i++) {
                        if (isOccupied(buttons[X1 + spacesMoved][Y2])) {
                            return false;
                        }
                    }
                    return true;
                } else {
                    for (int i = 1; i <= spacesMoved; i++) {
                        if (isOccupied(buttons[X1 - spacesMoved][Y2])) {
                            return false;
                        }
                    }
                    return true;
                }
            }
            else{
                if(isBlack(buttons[X2][Y2])!=isBlack(buttons[X1][Y1])){
                    if(isOccupied(buttons[X2][Y2])==isOccupied(buttons[X1][Y1])){
                        return true;
                    }
                    return false;
                }
                return false;
            }
        }

        if(buttons[X1][Y1].getTag().toString().contains("rook")){
            if(Y1 !=Y2){
                int spaceMoved = Math.abs(Y2-Y1);
                    if(Y2>Y1){
                        for(int i=1;i<spaceMoved;i++){
                            if(isOccupied(buttons[X1][Y1+i])){
                                return false;
                            }
                        }
                        return true;
                    }
                    else{
                        for(int i=1;i<spaceMoved;i++) {
                            if (isOccupied(buttons[X1][Y1 - i])) {
                                return false;
                            }
                        }
                        return true;
                    }
                }
            else{
                int spaceMoved = Math.abs(X2-X1);
                if(X2>X1){
                    for(int i=1;i<spaceMoved;i++){
                        if(isOccupied(buttons[X1+i][Y2])){
                            return false;
                        }
                    }
                    return true;
                }
                else{
                    for(int i=1;i<spaceMoved;i++) {
                        if (isOccupied(buttons[X1 -i][Y2])) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }

        if(buttons[X1][Y1].getTag().toString().contains("knight")){
            return true;
        }

        if(buttons[X1][Y1].getTag().toString().contains("bishop")){
            if(X2<X1){
                if(Y2<Y1){
                    int spacesMoved=Math.abs(X2-X1);
                    for(int i=1; i<spacesMoved;i++){
                            if (isOccupied(buttons[X1-i][Y1-i])){
                                return false;
                            }
                        }
                        return true;
                    }
                else{
                    int spacesMoved=Math.abs(X2-X1);
                    for(int i=1; i<spacesMoved;i++){
                        if (isOccupied(buttons[X1-i][Y1+i])){
                            return false;
                        }
                    }
                    return true;
                }
            }
            else{
                if(Y2<Y1){
                    int spacesMoved=Math.abs(X2-X1);
                    for(int i=1; i<spacesMoved;i++){
                        if (isOccupied(buttons[X1+i][Y1-i])){
                            return false;
                        }
                    }
                    return true;
                }
                else{
                    int spacesMoved=Math.abs(X2-X1);
                    for(int i=1; i<spacesMoved;i++){
                        if (isOccupied(buttons[X1+i][Y1+i])){
                            return false;
                        }
                    }
                    return true;
                }
            }
        }

        if(buttons[X1][Y1].getTag().toString().contains("queen")){
            if(X1==X2){
                int spaceMoved = Math.abs(Y2-Y1);
                if(Y2>Y1){
                    for(int i=1;i<spaceMoved;i++){
                        if(isOccupied(buttons[X1][Y1+i])){
                            return false;
                        }
                    }
                    return true;
                }
                else{
                    for(int i=1;i<spaceMoved;i++) {
                        if (isOccupied(buttons[X1][Y1 - i])) {
                            return false;
                        }
                    }
                    return true;
                }
            }
            if(Y1==Y2){
                int spaceMoved = Math.abs(X2-X1);
                if(X2>X1){
                    for(int i=1;i<spaceMoved;i++){
                        if(isOccupied(buttons[X1+i][Y2])){
                            return false;
                        }
                    }
                    return true;
                }
                else{
                    for(int i=1;i<spaceMoved;i++) {
                        if (isOccupied(buttons[X1 -i][Y2])) {
                            return false;
                        }
                    }
                    return true;
                }
            }

            if(X2<X1){
                if(Y2<Y1){
                    int spacesMoved=Math.abs(X2-X1);
                    for(int i=1; i<spacesMoved;i++){
                        if (isOccupied(buttons[X1-i][Y1-i])){
                            return false;
                        }
                    }
                    return true;
                }
                else{
                    int spacesMoved=Math.abs(X2-X1);
                    for(int i=1; i<spacesMoved;i++){
                        if (isOccupied(buttons[X1-i][Y1+i])){
                            return false;
                        }
                    }
                    return true;
                }
            }
            else{
                if(Y2<Y1){
                    int spacesMoved=Math.abs(X2-X1);
                    for(int i=1; i<spacesMoved;i++){
                        if (isOccupied(buttons[X1+i][Y1-i])){
                            return false;
                        }
                    }
                    return true;
                }
                else{
                    int spacesMoved=Math.abs(X2-X1);
                    for(int i=1; i<spacesMoved;i++){
                        if (isOccupied(buttons[X1+i][Y1+i])){
                            return false;
                        }
                    }
                    return true;
                }
            }

        }

        if(buttons[X1][Y1].getTag().toString().contains("king")){
            return true;
        }


        return true;
    }

    public boolean pawnValidMove(int x1, int y1, int x2, int y2){
        if(buttons[x1][y1].getTag().toString().contains("black")){
            if(y1==1){
                if(x2==x1 && ((y2==(y1+1))|| (y2==y1+2))){
                    return true;
                }
            }
            else if((y2==(y1+1))&& x2==x1){
                return true;
            }
            else if(y2==(y1+1)&&(Math.abs(x2-x1)==1)){
                return true;
            }
            return false;
        }
        else{
            return false;
        }
    }

    public boolean isBlack(Button b){
        if(b.getTag().toString().contains("black")&&!b.getTag().toString().contains("tile")){
            return true;
        }
        return false;
    }

    public boolean isTile(Button b){
        if(b.getTag().toString().contains("tile")){
            return true;
        }
        return false;
    }

    private boolean check(Button b){
        return true;
    }


}


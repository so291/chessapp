package com.example.chessapp;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.*;

public class SaveListAdapter extends ArrayAdapter<saveState> {
    private Context mContext;
    private int mResource;
    public SaveListAdapter(Context context, int resource, ArrayList<saveState> objects){
        super(context, resource, objects);
        mResource = resource;
        mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String name = getItem(position).name;
        String url = getItem(position).url;

        saveState ss = new saveState(name,url);

        LayoutInflater inflater =  LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView tvName = (TextView)convertView.findViewById(R.id.rowText);
        tvName.setText(name);

        return convertView;
    }

}
